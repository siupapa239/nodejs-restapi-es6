// const express = require('express')
// const bodyParser = require('body-parser')
// const routes = require("./routes/"); // index.js 는 / 와 같으므로 생략 가능
import express from "express"
import bodyParser from "body-parser"
import routes from "./routes/"

const app = express()
const port = 3000


app.use(bodyParser.json())
app.use((req, res, next) => {
    //인터셉터 역할 부여   
    //res.writeHead(200, {'Content-Type': 'application/json; charset=utf-8'});
    next();
});
app.use('/v1/api', routes)           // use 는 경로에 대한 확장성을 의미한다.

const sequelize = require('./models').sequelize
sequelize.sync()

app.listen(port, () => 
    console.log(`Example app listening at http://localhost:${port}`)
)