# express
- express 설치
mkdir api-template   
cd api-template   
npm init  
npm install express --save  

- index.js 생성
const express = require('express')  
const app = express()  
const port = 3000  
  
app.get('/', (req, res) => res.send('Hello World!'))  
  
app.listen(port, () => console.log(`Example app listening at http://localhost:${port}`))  

# babel설치(참고 URL - https://shlee0882.tistory.com/231)
npm install @babel/node --save
npm install @babel/preset-env --save
npm install @babel/core --save 

# babelrc 파일 생성, index.js 및 package.json 수정
- .babelrc 파일 생성 후 내용 추가  
{  
	"presets": ["@babel/preset-env"]  
}  

- index.js 수정
// const express = require('express');
import express from "express";

- package.json 수정
"scripts": {
    "start": "babel-node index.js"
}

# nodemon 설치
- pm install nodemon -D
- package.json 수정
"scripts": {
    "start": "nodemon --exec babel-node index.js"
}

# Logging - winston 설치
npm install --save winston
npm install --save app-root-path   //log 파일 경로 지정하기 위해 app-root-path 모듈을 이용했다.

# DB - sequlize, mysql2 
npm install --save sequelize
npm install --save mysql2




