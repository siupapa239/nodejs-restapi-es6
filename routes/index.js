// routes/index.js
// const router = require("express").Router();
// const main = require("./controller/main");
// const user = require("./controller/user");
import express from "express"
import main from "./controller/main"
import user from "./controller/member"

const router = express.Router();

/* main routing */
router.get("/index", main.index);
router.get("/main", main.index);
router.get("/", main.index);

/* user routing */
router.get("/member/index", user.index);   
router.get("/member/create", user.create);  // TODO - route.post로 변경예정(router.post('/user/create', controller.create))
router.get("/member/read", user.read);
router.get("/member/update", user.update);  // TODO - route.put로 변경예정(router.put('/user/update/:id', controller.update))
router.get("/member/delete", user.delete);  // TODO - route.delete로 변경예정(router.delete('/user/delete/:id', controller.delete))

// module.exports = router;
export default router;
