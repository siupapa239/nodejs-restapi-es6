// routes/controller.js
// exports.내보낼이름
// exports.mainView = function (req, res) {
//     res.end("Hello World")
// }
// import mysql from 'mysql'
// import dbconfig from '../../config/dbconfig'
// const dbconfig = require('../../config/dbconfig')
// const connection = mysql.createConnection(dbconfig)
var {User} = require('../../models');

const member = {

    index : function(req, res, next){

        // findall
        // User.findAll() 
        //     .then((users) => { 
        //         console.log(users[0].dataValues)
        //         res.end(users[0].useremail); 
        //     });
        
        // findOne
        User.findOne({where:{id:1}}) 
            .then((user) => { 
                // console.log(user.dataValues)
                // res.end(user.useremail); 

                return res.json(user);
            });

    },

    create : function(req, res){
        res.end('user create');
    },

    read : function(req, res){
        res.end('user read');
    },

    update : function(req, res){
        res.end('user update');
    },

    delete : function(req, res){
        res.end('user delete');
    }
}

module.exports = member;